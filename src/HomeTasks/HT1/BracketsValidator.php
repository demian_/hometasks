<?php

namespace App\HomeTasks\HT1;

class BracketsValidator
{
    private const CONSTRAINTS = [' ', "\n", "\t", "\r"];

    public function validateString(string $input)
    {
        $formattedString = str_replace(self::CONSTRAINTS, '', $input);

        return $this->areBracketsPaired($formattedString);
    }

    private function areBracketsPaired(string $input): bool
    {
        $stack = [];

        foreach (str_split($input) as $char) {
            if ($char === '(') {
                array_push($stack, $char);
            } elseif ($char === ')') {
                $lastChar = array_pop($stack);
                if (!$lastChar) {
                    return false;
                }
            } else {
                throw new \InvalidArgumentException('Wrong char found.');
            }
        }

        return count($stack) === 0;
    }
}
